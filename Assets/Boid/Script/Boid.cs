﻿using UnityEngine;

public class Boid : MonoBehaviour
{
	[Header("Fly Control")]
	public float zoneRepulsion = 5;
	public float zoneAlignement = 7;
	public float zoneAttraction = 50;

	[Space]
	public float forceRepulsion = 30;
	public float forceAlignement = 30;
	public float forceAttraction = 30;

	[Header("Purchase Control")]
	public float maxSpeed = 20;
	public float minSpeed = 12;
	public float forceTarget = 20;
	internal Vector3 target = new Vector3();
	internal bool goToTarget = false;
	internal Vector3 velocity = new Vector3();

	[Header("Land Control")]
	public float timeToRest = 2;
	public float landHeight = 0;
	private float _beforeLiftOff = 0;

	[Header("Gizmos")]
	[SerializeField]
	private bool drawGizmos = true;
	[SerializeField]
	private bool drawLines = true;

	[Header("Camera Control")]
	[SerializeField]
	private Transform cameraPivot;

	private bool _landed = false;
	[SerializeField] 
	private float collisionDistance = 5;

	// Update is called once per frame
	void Update()
	{
		//If rest not move
		if (_landed) {
			_beforeLiftOff -= Time.deltaTime;
			if (_beforeLiftOff <= 0)
				_landed = false;
			else
				return;
		}

		//Calcul trajectoire
		Vector3 sumForces = new Vector3();
		float nbForcesApplied = 0;

		foreach (Boid otherBoid in BoidManager.SharedInstance.roBoids) {
			//Ignore boid landed
			if (otherBoid._landed) continue;

			CalculInfluence(otherBoid, ref sumForces, ref nbForcesApplied);
		}

		//Si on a une target, on l'ajoute
		if (goToTarget) {
			Vector3 vecToTarget = target - transform.position;
			if (vecToTarget.sqrMagnitude < 1)
				goToTarget = false;
			else {
				Vector3 forceToTarget = vecToTarget.normalized * forceTarget;
				sumForces += forceToTarget;
				nbForcesApplied++;
				if (drawLines)
					Debug.DrawLine(transform.position, target, Color.magenta);
			}

			//Debug
			if (drawLines)
				Debug.DrawLine(transform.position, transform.position + sumForces, Color.magenta);
		}


		//On fait la moyenne des forces, ce qui nousrend indépendant du nombre de boids
		sumForces /= nbForcesApplied;

		//On freine
		velocity += -velocity * (10 * Vector3.Angle(sumForces, velocity)) / 180.0f * Time.deltaTime;

		//on applique les forces
		velocity += sumForces * Time.deltaTime;

		//On limite la vitesse
		if (velocity.sqrMagnitude > maxSpeed * maxSpeed)
			velocity = velocity.normalized * maxSpeed;
		if (velocity.sqrMagnitude < minSpeed * minSpeed)
			velocity = velocity.normalized * minSpeed;

		//Avoid Obstacles
		Ray ray = new Ray(transform.position, velocity);
		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, collisionDistance)) {
			//TODO Avoid correctly
			Debug.DrawRay(transform.position, velocity, Color.red);
			velocity += transform.right + transform.up;
			Debug.DrawRay(transform.position, velocity, Color.yellow);
		}

		//On regarde dans la bonne direction        
		if (velocity.sqrMagnitude > 0)
			transform.LookAt(transform.position + velocity);


		//Deplacement du boid
		var pos = transform.position;
		pos += velocity * Time.deltaTime;
		//LANDING
		if (pos.y <= landHeight) {
			transform.position = pos;
			transform.LookAt(pos + Vector3.forward);
			velocity = Vector3.zero;
			goToTarget = false;
			_landed = true;
			_beforeLiftOff = timeToRest;
		} else
			transform.position = pos;
	}

	private void CalculInfluence(Boid otherBoid, ref Vector3 sumForces, ref float nbForcesApplied)
	{
		Vector3 vecToOtherBoid = otherBoid.transform.position - transform.position;

		//Si on doit prendre en compte cet autre boid (plus grande zone de perception)
		if (vecToOtherBoid.sqrMagnitude < zoneAttraction * zoneAttraction) {
			//Si on est entre attraction et alignement
			Vector3 forceToApply;
			Color lineColor;
			if (vecToOtherBoid.sqrMagnitude > zoneAlignement * zoneAlignement) {
				//On est dans la zone d'attraction uniquement
				lineColor = Color.green;
				float distToOtherBoid = vecToOtherBoid.magnitude;
				float normalizedDistanceToNextZone =
					((distToOtherBoid - zoneAlignement) / (zoneAttraction - zoneAlignement));
				float boostForce = (4 * normalizedDistanceToNextZone);
				if (!goToTarget) //Encore plus de cohésion si pas de target
					boostForce *= boostForce;
				forceToApply = vecToOtherBoid.normalized * (forceAttraction * boostForce);
			} else if (vecToOtherBoid.sqrMagnitude > zoneRepulsion * zoneRepulsion) {
				//Alignement
				lineColor = Color.blue;
				forceToApply = otherBoid.velocity.normalized * forceAlignement;
			} else {
				//Repulsion
				lineColor = Color.red;
				forceToApply = RepulsionForce(vecToOtherBoid);
			}

			sumForces += forceToApply;
			nbForcesApplied++;

			//Debug
			if (drawLines) {
				Debug.DrawLine(transform.position, otherBoid.transform.position, lineColor);
			}
		}
	}

	private Vector3 RepulsionForce(Vector3 vecToOtherBoid)
	{
		//On est dans la zone de repulsion
		float distToOtherBoid = vecToOtherBoid.magnitude;
		float normalizedDistanceToPreviousZone = 1 - (distToOtherBoid / zoneRepulsion);
		float boostForce = (4 * normalizedDistanceToPreviousZone);
		return vecToOtherBoid.normalized * (-1 * (forceRepulsion * boostForce));
		;
	}

	internal void SetCamera(Camera pCamera)
	{
		Transform cameraTransform = pCamera.transform;
		cameraTransform.parent = cameraPivot;
		cameraTransform.position = cameraPivot.position;
		cameraTransform.LookAt(cameraTransform.position + transform.forward);
	}

	internal void GoLand()
	{
		target = new Vector3(transform.position.x, landHeight, transform.position.z);
	}

	void OnDrawGizmosSelected()
	{
		if (drawGizmos) {
			var transformPosition = transform.position;
			// Répulsion
			Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(transformPosition, zoneRepulsion);
			// Alignement
			Gizmos.color = Color.green;
			Gizmos.DrawWireSphere(transformPosition, zoneAlignement);
			// Attraction
			Gizmos.color = Color.blue;
			Gizmos.DrawWireSphere(transformPosition, zoneAttraction);
		}
	}
}