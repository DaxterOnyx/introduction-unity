﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class BoidManager : MonoBehaviour
{
    private static BoidManager Instance = null;
    public static BoidManager SharedInstance
    {
        get
        {
            if (Instance == null)
            {
                Instance = GameObject.FindObjectOfType<BoidManager>();
                if (Instance == null)
                {
                    var go = new GameObject();
                    Instance = go.AddComponent<BoidManager>();
                }
            }
            return Instance;
        }
    }

    [SerializeField]
    private Camera myCamera;

    public Boid prefabBoid;
    public float nbBoids = 100;
    public float startSpeed = 1;
    public float startSpread = 10;

    public float maxDistBoids = 30;

    public float periodRetargetBoids = 6;
    public float probaLandBoids = 0.5f;
    public float periodNoTargetBoids = 3;
    private float timerRetargetBoids = 0;
    private bool setTargetToBoids = true;

    private List<Boid> boids = new List<Boid>();
    public ReadOnlyCollection<Boid> roBoids
    {
        get { return new ReadOnlyCollection<Boid>(boids); }
    }

    void Start()
    {
        for (int i = 0; i < nbBoids; i++)
        {
            Boid b = GameObject.Instantiate<Boid>(prefabBoid);
            Vector3 positionBoid = (Random.insideUnitSphere * startSpread);
            positionBoid.y = Mathf.Abs(positionBoid.y)+4; //Ne pas créer des oiseaux sous 0, on imagine que ce sera le sol.
            b.transform.position = positionBoid;
            b.velocity = (positionBoid - transform.position).normalized * startSpeed;
            b.transform.parent = this.transform;
            b.maxSpeed *= Random.Range(0.95f, 1.05f);
            boids.Add(b);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            boids[0].SetCamera(myCamera);
        }

        //Décrémente la temporisation
        timerRetargetBoids -= Time.deltaTime;
        if (timerRetargetBoids <= 0)
        {
            if (!setTargetToBoids)
                timerRetargetBoids = periodNoTargetBoids;
            else
                timerRetargetBoids = periodRetargetBoids;

            Vector3 target = Random.insideUnitSphere * maxDistBoids;

            //Land on floor
            if (Random.Range(0, 1f) < probaLandBoids)
            {
                foreach (var item in boids)
                {
                    item.GoLand();
                }
            }
            else
            {

                target.y = Mathf.Max(Mathf.Abs(target.y), 10);
                foreach (Boid b in boids)
                {
                    b.goToTarget = false;
                    if (setTargetToBoids && Random.Range(0.0f, 1.0f) < 0.3f)
                    {
                        b.target = target;
                        b.goToTarget = true;
                    }
                }
            }

            setTargetToBoids = !setTargetToBoids;
        }
    }
}