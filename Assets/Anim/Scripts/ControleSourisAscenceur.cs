﻿using UnityEngine;

public class ControleSourisAscenceur : MonoBehaviour
{
	private Animator animator;

	private static readonly int Monter = Animator.StringToHash("Monter");
	private static readonly int Descendre = Animator.StringToHash("Descendre");

	// Start is called before the first frame update
	void Start()
	{
		animator = GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update()
	{
		if (Input.GetButtonDown("Fire1"))
		{
			animator.SetTrigger(Monter);
			animator.ResetTrigger(Descendre);
		}

		if (Input.GetButtonDown("Fire2"))
		{
			animator.ResetTrigger(Monter);
			animator.SetTrigger(Descendre);
		}
	}
}