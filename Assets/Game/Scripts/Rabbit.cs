﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Rabbit : Controller
{
	[Header("Move")] [SerializeField] private Transform cameraPivot;
	[SerializeField] private float speed = 1;
	[SerializeField] private float rotateSpeed = 1;
	[SerializeField] private float rotateMax = 90;
	[SerializeField] private float rotateMin = -90;
	private new Rigidbody rigidbody;
	private Vector2 _velocity = Vector2.zero;
	private Vector2 _rotation = Vector2.zero;
	[Header("Jump")] [SerializeField] private float jumpForce = 5;
	[SerializeField] private float heightPlayer = 2; 
	[SerializeField] private float floorDetectionDist = 1f;
	private bool _canJump => Physics.Raycast(transform.position - new Vector3(0,heightPlayer/2,0), Vector3.down, floorDetectionDist);
	private bool _isChargingJump = false;
	private float _startChargingJump;
	// [Header("Anim")]
	private Animator _animator;

	private void Start()
	{
		rigidbody = GetComponent<Rigidbody>();
		_animator = GetComponent<Animator>();
	}

	public void Move(InputAction.CallbackContext context)
	{
		_velocity = context.ReadValue<Vector2>();
	}

	public void Look(InputAction.CallbackContext context)
	{
		_rotation += context.ReadValue<Vector2>() * rotateSpeed * Time.deltaTime * Mathf.Rad2Deg;
		//Clamp y rotation
		if (_rotation.y > 0 && _rotation.y > rotateMax)
		{
			// Debug.Log("TOP " + nextRotation);
			_rotation.y = rotateMax;
			return;
			// _rotation.y = rotateMax - cameraPivotRotation;
		}
		else if (_rotation.y < 0 && _rotation.y < rotateMin)
		{
			//Debug.Log("BOTTOM " + nextRotation);
			_rotation.y = rotateMin;
			// cameraPivot.LookAt(cameraPivot.up * -1);
			return;
			// _rotation.y = 360 + rotateMin - cameraPivotRotation;
		}
		// cameraPivot.localRotation = Quaternion.Euler(-_rotation.y * rotateSpeed * Time.deltaTime,0,0);

		//Mousse up and down
		cameraPivot.localRotation = Quaternion.Euler(-_rotation.y, 0, 0);

		//mouse left and right
		transform.localRotation = Quaternion.Euler(0, _rotation.x, 0);
	}

	public void Fire(InputAction.CallbackContext context)
	{
		Debug.Log(context.control);
	}

	public void Jump(InputAction.CallbackContext context)
	{
		Debug.Log(context);
		if (context.started)
		{
			// Debug.Log("Started");
			_startChargingJump = (float) context.startTime;
			_isChargingJump = true;
			_animator.SetBool("IsChargingJump", true);
		}

		if (context.canceled)
		{
			float deltaTimeChargingJump = (float) (context.startTime - _startChargingJump);
			// Debug.Log("Canceled = " + deltaTimeChargingJump);
			_isChargingJump = false;
			_animator.SetBool("IsChargingJump", false);
			if (_canJump)
			{
				Vector3 vecJumpForce = (transform.up + transform.forward).normalized * jumpForce * deltaTimeChargingJump;
				rigidbody.AddForce(vecJumpForce, ForceMode.VelocityChange);
				_animator.SetBool("Jumping", true);
			}
		}
	}


	private void Update()
	{
		//MOVE
		transform.Translate(new Vector3(_velocity.x, 0, _velocity.y) * (speed * Time.deltaTime));
	}

	private void OnCollisionEnter(Collision other)
	{
		if (_animator.GetBool("Jumping"))
			_animator.SetBool("Jumping", false);
	}

	private void OnDrawGizmos()
	{
		if (_canJump)
			Gizmos.color = Color.green;
		else
			Gizmos.color = Color.red;
		
		Gizmos.DrawRay(transform.position-new Vector3(0,heightPlayer/2,0),-1*transform.up);
	}
}